// list accounts
eth.accounts

// get balance in wei on account 1
eth.getBalance(eth.accounts[0])
// same but result in ether
web3.fromWei(eth.getBalance(eth.accounts[0]), "ether")

// send ether, here 42 wei
//eth.sendTransaction({from: '0xSomeAdress', to: '0xOtherAddressWhyDoYouReadThis???', value: web3.toWei(42, "wei")})
//eth.sendTransaction({from: eth.accounts[0], to: eth.accounts[1], value: web3.toWei(42, "wei")})

// send everything
var everything = eth.getBalance(eth.accounts[0]) - web3.eth.gasPrice*21000
//eth.sendTransaction({from: eth.accounts[0], to: "0xe62fcfbe69189c9fc537b5209d633b9e4973f2d1", value: everything, gas: 21000})



//eth.sendTransaction({from: eth.accounts[0], to: eth.accounts[1], value: web3.toWei(36, "milliether")})


// convert
var price = 0.00 // btc/eth
var ether = 0.14157819999999988
var btc = ether * price

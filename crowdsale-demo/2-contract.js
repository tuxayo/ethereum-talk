// js extension used to get syntax coloration
// contract to manage a crowdfunding
// written in Solidity, a language based on JavaScript
contract Crowdsale {
    // declaration of persisted date (contract state)
    address public beneficiary;
    uint public fundingGoal; uint public amountRaised; uint public deadline;
    Funder[] public funders;
    event FundTransfer(address backer, uint amount, bool isContribution);

    /* data structure to hold information about a campaign contributor */
    struct Funder {
        address addr;
        uint amount;
    }

    // initialization, called when sending the contrat on the network
    // to setup contract parameters
    function Crowdsale(address _beneficiary, uint _fundingGoal, uint _duration) {
        beneficiary = _beneficiary;
        fundingGoal = _fundingGoal;
        deadline = now + _duration * 1 minutes;
    }

    /* The function without name is the default function that is called whenever
     anyone sends funds to a contract */
    function () { // here it stores who contributed and how much
        uint amount = msg.value;
        funders[funders.length++] = Funder({addr: msg.sender, amount: amount});
        amountRaised += amount;
        FundTransfer(msg.sender, amount, true);
    }

    // condition to restrict function execution
    // here, only when crowdfunding ended
    modifier afterDeadline() { if (now >= deadline) _ }

    /* checks if the goal or time limit has been reached and ends the campaign */
    function checkGoalReached() afterDeadline {
        if (amountRaised >= fundingGoal){ // transfert funds to the projects owner
            beneficiary.send(amountRaised); // if the target amount is reached
            FundTransfer(beneficiary, amountRaised, false);
        } else { // in case of failure of the crowdfunding
            FundTransfer(0, 11, false);
            for (uint i = 0; i < funders.length; ++i) {
              funders[i].addr.send(funders[i].amount); // send back to contributors
              FundTransfer(funders[i].addr, funders[i].amount, false);
            }
        }
        // disable contract and send the eventual
        // remaining ether/money to a given address
        suicide(beneficiary);
    }
}

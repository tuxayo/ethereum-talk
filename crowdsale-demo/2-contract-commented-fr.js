// js extension used to get syntax coloration
// contrat qui gére un financement participatif (crowdfunding)
// écrit dans le langage Solidity, basé sur JavaScript
contract Crowdsale {
    // déclarations des données persistées (état du contrat)
    address public beneficiary; // porteur de projet
    uint public fundingGoal; uint public amountRaised; uint public deadline;
    Funder[] public funders;
    event FundTransfer(address backer, uint amount, bool isContribution);

    // structure de données pour représenter un contributeur
    struct Funder {
        address addr;
        uint amount;
    }

    // initialisation, appelée au moment de l'envoi du contrat sur le réseau
    // afin d'y inclure les paramètres du contrat
    function Crowdsale(address _beneficiary, uint _fundingGoal, uint _duration) {
        beneficiary = _beneficiary;
        fundingGoal = _fundingGoal;
        deadline = now + _duration * 1 minutes;
    }

    // fonction par defaut (appelée à chaque envoie d'argent au contrat)
    function () { // stocke qui a donné et combien
        uint amount = msg.value;
        funders[funders.length++] = Funder({addr: msg.sender, amount: amount});
        amountRaised += amount;
        FundTransfer(msg.sender, amount, true);
    }

    // condition qui permet de restreindre l'execution des fonctions
    // ici, execute uniquement après la fin du crowdfunding
    modifier afterDeadline() { if (now >= deadline) _ }

    // fonction utilisée pour finaliser le crowdfunding une fois le delais écoulé
    function checkGoalReached() afterDeadline {
        if (amountRaised >= fundingGoal){   // vire l'argent au porteur de
            beneficiary.send(amountRaised); // projet si le montant est atteint
            FundTransfer(beneficiary, amountRaised, false);
        } else { // en cas d'échec, renvoie l'argent aux contributeurs
            FundTransfer(0, 11, false);
            for (uint i = 0; i < funders.length; ++i) {
              funders[i].addr.send(funders[i].amount);
              FundTransfer(funders[i].addr, funders[i].amount, false);
            }
        }
        // désactive le contrat et revoie l'éventuel solde restant à une adresse donnée
        suicide(beneficiary);
    }
}
